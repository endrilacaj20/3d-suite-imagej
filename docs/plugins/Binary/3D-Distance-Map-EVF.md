## 3D Distance Map and _Layers_  
This plugin will compute the 3D Euclidean Distance Map (EDM) in 3D using calibrated units from a binary or labelled image. In this map image, the value of the pixel will be the euclidean distance to the borders of the objects present in the original image. The EDM can be computed inside an object or outside.

![Raw image](../../imgs/groom-raw.png)

Original image

![Binary image](../../imgs/groom-bin.png)

Thresholded binary image

![EDT](../../imgs/groom-edt.png)

Euclidean Distance Map of the image

## Layers and EVF
In order to get layers of equal volume to quantify spatial distribution, the distance values in the EDM image must be normalised. The normalised image is then called EVF (Eroded Volume Fraction) and will define layers inside (or outside).

![EDT](../../imgs/groom-evf.png)

EVF or layers of the image

