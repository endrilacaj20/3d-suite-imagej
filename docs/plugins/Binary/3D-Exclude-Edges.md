## 3D Exclude Edges
This plugin will take a labelled image and remove objects touching _X_ and _Y_ edges, optionnaly objects touching the _Z_ edges can also be removed. 

![exclude edges before](../../imgs/3d-exclude-edges-1.png)

Before removing objects on edges

![exclude edges after](../../imgs/3d-exclude-edges-2.png)

After removing objects on edges


