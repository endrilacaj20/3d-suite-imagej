## 3D ROI Manager Options

This plugin allows to manage 3D ROIs and perform various interactive measurements. There are many options to select the different measurements to perform, and for segmentation. 

### Measurements

You can choose the different **measurements** to perform, first for **geometry** and **shape**, available with _Measure3D_.

- **Volume**: the volumes of the object in voxel and in calibrated unit. 

- **Surface**: the surface area of a 3D object, in voxel and calibrated unit, along with the _corrected surface_ in voxel unit.

- **Compactness**: the compactness and sphericity of the object, as a ration between surface and volume, along with alternative measurements for compactness.

- **Fit Ellipse**: after best fitting of an ellipsoid, different measurements, such as length the major radius in unit, the ellipsoid elongation and flatness, and the volume of the ellipsoid.

- **3D Moments**: the first 5 moments of the 3D shape.

- **Convex Hull**: the volume of the corresponding convex hull, it may take time to compute.

- **Feret**: the 3D Feret diameter in unit, as the longest distance between two points of the object, it may take time to compute for large objects.

- **Centroid(pix)**: the coordinates of the 3D centroid of the object, in pixel unit.

- **Centroid(unit)**: the coordinates of the 3D centroid of the object, in calibrated unit.

- **Distance to surface**: the statistics for the distances between the centre of the object and its contour, mean, minimum, maximum and Standard Deviation (SD). If the centre of the object is outside the object, _NaN_ values will be returned. 

- **Bounding Box**: the different coordinates of the bounding box of the object, along with the bounding volume.

### Quantification 

You can choose the different intensity and values **quantification** to perform with _Quantif3D_ for the current image stack.

- **Integrated density**: the sum of all values of the current image stack within the object. 

- **Mean**: the average of all values of the current image stack within the object. 

- **Minimum**: the minimum of all values of the current image stack within the object.

- **Maximum**: the maximum of all values of the current image stack within the object.

- **Std Dev**: the standard deviation of all values of the current image stack within the object.

- **Mode**: the most abundant of all values of the current image stack within the object, along with the non-zero most abundant of all values.

- **Centre of Mass(pix)**: the coordinates in pixel unit of the weighted 3D centroid of the object, using values in the current image as weights.

- **Centre of Mass(unit)**: the coordinates in calibrated unit of the weighted 3D centroid of the object, using values in the current image as weights.

- **Objects Numbering**: in case the current image stack is an image with labelled objects, the number of objects from the current image within the object, along with the total volume occupied by these objects.

### Distances

The following options are available for **distances**.

- **Radial distance**: the distance from the centre of the object to its border passing trough the centre of the object is computed and giving the _radiusCen_ value. The normalised distance from the centre or the border to the centre of the other object is then given by the _excen_ and _periph_ values. 

- **Closest object**: this will compute the closest object within the whole list, considering centre-centre distance or border-border distance. If there is a large number of objects, it may take time to compute.

### Colocalisation

The following option is available for **colocalisation**.

- ** Surface contact**: This will compute, based on a defined distance, the contact surface between the two objects.

### Options

The different options are available for **segmentation** and **labelling**.

- **Use 32-bit image**: the resulting image from _3D segmentation_ will be 32-bit.

- **Exclude XY**: when using _Add Image_ the objects touching _X_ or _Y_ edge will be discarded.

- **Exclude Z**: when using _Add Image_ the objects touching _Z_ edge will be discarded.

- **Sync 3D viewer**: selected object in 3D viewer will be selected in 3D Manager (does not always work).

- **Split options**: the minimal distance between the two nen computed centres.

- **Surface contact**: the maximal distance between the borders of the two objects.

- **Overlay**: how to display the overlay using _LiveROI_.

- **New UI**: experimental new UI.

- **Multiple**: multiple instances of 3D Manager can be opened.











