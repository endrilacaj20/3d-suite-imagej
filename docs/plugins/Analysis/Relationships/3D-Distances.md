## 3D Distances

This plugin  will compute distances between objects in a same image, or between objects in two separate images.  

You will need to specify the first image, named _A_, and the second image, named _B_, the two images _A_ and _B_ can be the same. 

You will have to choose if you want to measure **all** the distances between all pairs of objects, or only the distances to the **closest(s)** object(s). 

In case image _A_ and _B_ are the same, the first closest object will always be itself, so you should choose _closest2_ to get the closest object. For objects in two separate images, you can choose _closest1_ to get the first closest object, or _closest2_ to get the first two closest objects. The _closest_ object can be computed using centre-to-centre distances or border-to-border distances, the later distance will take longer to compute.

The distances that can be computed are: centre-to-centre, border-to-border, or [Hausdorff](https://en.wikipedia.org/wiki/Hausdorff_distance). The border-to-border and Hausdorff distances will take longer to compute.

For the computation of **all** distances, the values in the result table are the computed distances for:

- **Label**: the value of object in image _A_
- **Bi**: the value of object in image _B_

A separate result table will also display basic statistics about the computed distances. 

For the computation of **closest** distances, the values in the result table are:*

- **Label**: the value of object in image _A_
- **Closest_1**: the value of the object for first closest object in image _B_.
- **Distance_1**: the distance to the object for first closest object in image _B_. If images _A_ and _B_ are the same, this distance should be _0_.
- **Closest_2**: the value of the object for second closest object in image _B_.
- **Distance_1**: the distance to the object for second closest object in image _B_.



