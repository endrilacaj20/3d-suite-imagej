## 3D Interactions

With this plugin you can detect and quantify the interactions with touching or almost touching objects defined in a labelled image.

The interactions can be defined in 3 different manners: 

- **Lines**: there is a line of blck pixels (with value 0) separating the objects
- **Touching**: the pixels of one object are touching pixels of another object
- **Dilation**: the objects are touching after a dilation specified (with radii in pixels)

![Interaction1](../../../imgs/interaction-lines.png)
![Interaction2](../../../imgs/interaction-touching.png)

The values in the results table are:

- **Value**: the label of the object in the image
- **Oi**: the label of the _ith_ object interacting
- **Vi**: the interaction surface area for the _ith_ object





