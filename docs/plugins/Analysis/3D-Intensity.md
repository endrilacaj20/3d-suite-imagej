## 3D Intensity quantification

This plugin will compute the basic statistics about objects intensity quantification.

The plugin will require two images as input, the first image will contain the **objects** inside which to perform intensity measurement, the other one will contain the **signal** to be quantified. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

The values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value of the object in the labelled image
- **Average**: the mean value of the signal inside the object
- **Minimum**: the minimum value of the signal inside the object
- **Maximum**: the maximum value of the signal inside the object
- **StandardDeviation**: the standard deviation of the values of the signal inside the object
- **IntegratedDensity**: the sum of  value of the values of the signal inside the object
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image
