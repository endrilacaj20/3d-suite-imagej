## 3D Centroid quantification

This plugin will compute the centroid (geometrical centre) coordinates of the objects.

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

The values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value of the object in the labelled image
- **CX(pix)**: the coordinate _X_ of the centroid of the object
- **CY(pix)**: the coordinate _Y_ of the centroid of the object
- **CZ(pix)**: the coordinate _Z_ of the centroid of the object
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image 
