## 3D Surface quantification from mesh

This plugin will compute the surface of the object as a mesh, the surface tends to be smoother.

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed. 

- **Label**: the name of the image
- **Value**: the pixel value
- **SurfaceArea**: the mesh surface in pixel unit
- **SurfaceAreaSmooth**: the smoothed mesh surface in pixel unit
