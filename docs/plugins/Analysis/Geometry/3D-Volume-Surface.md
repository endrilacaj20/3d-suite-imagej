## 3D Volume and surface quantification

This plugin will compute the basic measurements about objects volume and surfaces.

The plugin will require one images as input, the will contain the **objects** to measure. The **objects** image can be a binary or labelled image, in case of a binary image, a labelling will be performed.

The values in the results table are :

- **Label**: the name of the image
- **Value**: the pixel value
- **Channel**: the channel number of the image
- **Frame**: the frame number of the image of the object in the labelled image

Then the volume of the object in pixel and calibrated unit.

- **Volume(pix)**: the volume of the object in pixel unit (the number of voxels of the object)
- **Volume(unit)**: the volume of the object in calibrated unit (the number of voxels of the object _times_ the volume of one voxel)

And the surfaces computed with different algorithms.

- **Surface(pix)**: the surface exposed in pixel unit
- **Surface(unit)**: the surface exposed in calibrated unit
- **SurfaceCorrected(pix)**: the corrected (smoothed) surface in pixl unit
- **SurfaceNb**:the number of contour voxels of the object

 
