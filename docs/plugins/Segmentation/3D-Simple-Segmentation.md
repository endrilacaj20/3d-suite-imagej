## 3D Simple segmentation and labelling
This plugin will threshold and label an image with bright objects over a dark background, it can also be used on an already thresholded image to detect and label objects in 3D.

The **low threshold** parameter is used for thresholding, if the image is already thresholded used the default value 128 (in the thresholded image, background pixels should have a value of _0_, and objects pixels _255_.

The plugin can also discard small or large objects using the parameters **min size** and **max size**. These sizes refer to pixels number, use default value _-1_ not to put a limit on maximum size. 

In case objects are only one pixel size objects, select the **individual voxels** option, each voxel will be a different object. In case you are expecting more than 65,535 objects, use the **32-bit segmentation** option to create a 32-bit segmentation image. 
 
