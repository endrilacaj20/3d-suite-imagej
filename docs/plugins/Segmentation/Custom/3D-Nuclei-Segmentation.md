## 3D Nuclei Segmentation

This plugin is designed to segment nuclei from cell culture (not from tissues). The method is based on a maximum Z-projection followed by a 2D Segmentation. The segmentation for the 2D projection is based on a global thresholding. The nuclei are then segmented and separated using ImageJ watershed. 

![Nuclei Seg1](../../../imgs/deepseg1.png)
![Nuclei Seg2](../../../imgs/deepseg2.png)

The zones created around each nuclei are then expanded in 3D, the individual nuclei are then segmented in the different zones using the same global thresholding method.

![Nuclei Seg3](../../../imgs/deepseg3.png)
![Nuclei Seg4](../../../imgs/deepseg4.png)

