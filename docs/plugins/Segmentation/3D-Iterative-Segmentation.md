## 3D Iterative Thresholding
The plugin will basically **test all thresholds** and detect objects for all thresholds, it will then try to build a _lineage_ of the objects detected, linking them from one  threshold to the next threshold, taking possible splits into account. So **different objects** can be segmented with **different thresholds**, the plugin allows various criteria to pick the best threshold :

  * **Elongation** : the threshold leading to the most round object is chosen (minimal elongation).
  * **Volume** : the threshold leading to the largest object.
  * **MSER** : the threshold where the volume of the object is most stable (minimal variation).
  * **Edges** : the threshold where the objects has maximal edges.

The other parameters are related to the** minimal and maximal volumes** of the objects that are to be detected. The **thresholds** tested can be tuned with 3 options with the _value_ parameter : 

  * **Step** : threshold are tested each step _value_. 
  * **Kmeans** : histogram is analysed and clustered into _value_ classes using a KMeans algorithm.
  * **Volume** : a constant number of pixels between two thresholds using _value_ thresholds. 

For **8-bits** images it is recommended to use the method _Step_ with _value_ between 1 and 5. For **16-bits** images try _Step_ with values between 5 and 100 depending on the dynamic of your data. Note than the more threshold tested the more memory used.

In order not to test low thresholds you can specify to start with the **mean value** of the image as the lowest threshold or specify manually the lowest threshold to start with. 

The image can be **filtered** before thresholding with a 3D median filter with radii proportional to the minimal volume. The **contrast** refers to the range of thresholds where the object exists, noise or very faint objects may have very low contrast as opposed to very contrasted object. 

![3d-iterative-1](../../imgs/dotblot.png)

Iterative thresholding using different criteria, bottom left elongation, top right volume and bottom right MSER.

Testing all thresholds may lead to **objects being divided into smaller objects** for high thresholds. For instance **touching cells** may result in close nuclei, at low contrast and low threshold the two nuclei may seem like touching and form only one object, however at high threshold two separate objects are  being seen. 

![3d-iterative-2](../../imgs/montagetouching.png)

Dividing objects with thresholds, top left raw image with high brightness, top right raw image with adjusted contrast to distinguish the dividing nuclei, bottom left first channel of Iterative thresholding showing brighter and smaller objects, bottom right second channel of Iterative thresholding showing merged nuclei for lower threshold.

If you find this plugin useful for your work, please refer to [3D ImageJ Suite](/3d-suite-imagej/) and cite this paper :  [A generic classification-based method for segmentation of nuclei in 3D images of early embryos](http://www.biomedcentral.com/1471-2105/15/9).

