import numpy as np
import jpype.imports

# convert from numpy array image to 3D Suite ImageHandler
def toImageHandler(image, comment=""):
    imX = image.shape[2]
    imY = image.shape[1]
    imZ = image.shape[0]
    imXY = imY * imX

    slicesList = []
    for i in range(0, imZ):
        slice = image[i, :, :]
        slice = slice.flatten()
        slicesList.append(slice)
    imageTemp = np.stack(slicesList, axis=0)
    imageNew = np.zeros((imZ, imXY), dtype='uint16')
    imageNew = imageNew + imageTemp

    from mcib3d.image3d import ImageShort, ImageByte, ImageFloat
    if image.dtype == 'uint8':
        handler = ImageByte(imageNew, comment, imX)
        return handler
    if image.dtype == 'uint16':
        handler = ImageShort(imageNew, comment, imX)
        return handler
    if image.dtype == 'float32':
        handler = ImageFloat(imageNew, comment, imX)
        return handler
    print("cannot handle " + str(image.dtype))
    return 0

def fromImagePlus(plus):
    from mcib3d.image3d import ImageHandler
    handler = ImageHandler.wrap(plus)
    return fromImageHandler(handler)


def toImagePlus(image, comment=""):
    handler = toImageHandler(image, comment)
    return handler.getImagePlus()


def fromImageHandler(handler):
    imX = handler.sizeX
    imY = handler.sizeY
    imZ = handler.sizeZ
    imXY = imY * imX
    # to numpy image
    arr3D = []
    from ij import ImagePlus
    if handler.getType() == ImagePlus.GRAY8:
        type = 'uint8'
    if handler.getType() == ImagePlus.GRAY16:
        type = 'uint16'
    if handler.getType() == ImagePlus.GRAY32:
        type = 'float32'
    for i in range(0, imZ):
        arr11 = np.zeros(imXY, dtype=type)
        arr1D = handler.getArray1D(i)
        arr11 = arr11 + arr1D
        arr11 = arr11.astype(type)
        arr2D = np.split(arr11, imY)
        arr3D.append(arr2D)
    imageTemp = np.stack(arr3D, axis=0)
    imageNew = np.zeros((imY, imX), dtype=type)
    imageNew = imageNew + imageTemp

    return imageNew